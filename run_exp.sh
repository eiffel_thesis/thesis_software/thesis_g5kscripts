#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

if [ $# -ne 1 ]; then
	echo "Usage: ${0} command_to_run" 1>&2

	exit 1
fi

# Run the given command through ssh.
# Since host can be unknown it is likely that a message like "The authenticity
# of host '...' can't be established." will be printed. The problem with this
# print is that it comes with a yes/no question to answer. To avoid that we use
# the option -oStrictHostKeyChecking=no.
ssh -oStrictHostKeyChecking=no -p10022 root@localhost "${1}";