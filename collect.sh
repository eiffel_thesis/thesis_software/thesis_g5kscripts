#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

USER='flaniel'
DIR='/tmp/tmp'
NFS_DIR='/home/flaniel'
IMG='/tmp/scripts/qemu-image.img'

if [ $EUID -ne 0 ]; then
	echo "${0} must be run as root!" 1>&2

	exit 1
fi

if [ ! -d $DIR ]; then
	mkdir $DIR
fi

# Mount image in temporary directory.
mount $IMG $DIR

# We will copy everything in $DIR/root while it is not share since share is the
# virtfs directory to share file with outside the VM.
# Normally it should only have one directory: the one which contains results of
# the just finished experiment.
# ls -d prints directory name, combined with $DIR/root/* it permits to print
# full path of directories.
for directory in $(ls -d $DIR/root/* | grep -v 'share'); do
	# We need to su in the proprietary of the NFS_DIR to be able to copy file in
	# it.
	su ${USER} -c "cp -r ${directory} ${NFS_DIR}"
done

# Umount the image and remove the temporary directory.
umount $DIR
rmdir $DIR