#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

WORK='/tmp'
LINUX='linux'
LINUX_BOOT='arch/x86/boot'

NFS_DIR='/home/flaniel'

for d in $WORK "${WORK}/${LINUX}" "${WORK}/${LINUX}/${LINUX_BOOT}"; do
	if [ ! -d $d ]; then
		mkdir -p $d
	fi
done

# We take everything we need to run experiments from the NFS home dir and copy
# into /tmp (since /tmp is big).
for i in 'exps' 'images' 'scripts' 'g5k_scripts'; do
	cp -r $NFS_DIR/$i $WORK
done

# linux directory is huge so we just take what we need.
cp $NFS_DIR/$LINUX/$LINUX_BOOT/bzImage_vanilla $WORK/$LINUX/$LINUX_BOOT
cp $NFS_DIR/$LINUX/$LINUX_BOOT/bzImage_modified $WORK/$LINUX/$LINUX_BOOT

sync