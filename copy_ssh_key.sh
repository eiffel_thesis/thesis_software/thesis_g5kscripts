#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

IMG='/tmp/scripts/qemu-image.img'
DIR='tmp'

PRIVATE_KEY='/root/.ssh/id_rsa'
PUBLIC_KEY='/root/.ssh/id_rsa.pub'

if [ $EUID -ne 0 ]; then
	echo "${0} must be run as root!" 1>&2

	exit 1
fi

# We create a dummy key which will be added to the VM as authorized key so we
# will be able to ssh.
ssh-keygen -f $PRIVATE_KEY -N ''

if [ ! -d $DIR ]; then
	mkdir $DIR
fi

# Mount image in temporary directory.
mount $IMG $DIR

# Once the image is mounted we add the key to the authorized ones.
cat $PUBLIC_KEY >> $DIR/root/.ssh/authorized_keys

# Umount the image and remove the temporary directory.
umount $DIR
rmdir $DIR