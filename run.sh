#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

WORK='/tmp'
NFS_DIR='/home/flaniel'

BZIMAGE_VANILLA="${WORK}/linux/arch/x86/boot/bzImage_vanilla"
BZIMAGE_MODIFIED="${WORK}/linux/arch/x86/boot/bzImage_modified"

EXPS_PATH='share/tmp/exps/two_containers/sysbench/extented_test_soft_limit_vmachine'

EXPS=("${BZIMAGE_VANILLA} 'cd ${EXPS_PATH}; python3 pair_default.py'"
"${BZIMAGE_VANILLA} 'cd ${EXPS_PATH}; python3 pair_max_limit.py 1800M 1000M'"
"${BZIMAGE_VANILLA} 'cd ${EXPS_PATH}; python3 pair_soft_limit.py 1800M 1000M'"
"${BZIMAGE_MODIFIED} 'cd ${EXPS_PATH}; python3 pair_mechanism.py 1800M 1000M 2'"
)

# Setup a node to be ready to run experiment.
# @param node The node to setup, it need to be already reserved.
function setup {
	local node

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the node" 1>&2

		exit 1
	fi

	node=$1

	# We need to get the benchmark environment first.
	ssh root@${node} "bash ${NFS_DIR}/g5k_scripts/get_bench_env.sh"

	# We need to copy ssh key into image to be able to connect later.
	ssh root@${node} "bash ${WORK}/g5k_scripts/copy_ssh_key.sh"

	# Install qemu to be able to run the VM.
	ssh root@${node} "apt-get -q update"
	ssh root@${node} "apt-get -qy install qemu-system-x86"
}

# Run an experiment on a node.
# @param node The node where the experiment will be run.
# @param bzImage The kernel bzImage to use for experiment.
# @param cmd The command to run the experiment.
function run {
	local node
	local bzImage
	local cmd

	if [ $# -lt 3 ]; then
		echo "${FUNCNAME[0]} needs three arguments: the node, the bzImage and the command to run" 1>&2

		exit 1
	fi

	node=$1
	bzImage=$2

	# We get the two first arguments so we need to remove them from $@ with shift.
	shift 2

	# The command to execute is the rest of the argument of this function.
	cmd=$@

	# Boot the VM with the given bzImage.
	ssh root@${node} "bash ${WORK}/scripts/run_extern_kernel.sh ${bzImage}"

	ssh root@${node} "bash ${WORK}/g5k_scripts/run_exp.sh ${cmd}"

	# Switch off the VM.
	# NOTE For information about '-oStrictHostKeyChecking=no' see run_exp.sh.
	ssh root@${node} "ssh -oStrictHostKeyChecking=no -p10022 root@localhost 'poweroff'"
}

# Collect all results for the given node.
# @param node The node where results directory will be collected.
function collect {
	local node

	if [ $# -ne 1 ]; then
		echo "${FUNCNAME[0]} needs one argument: the node" 1>&2

		exit 1
	fi

	node=$1

	ssh root@$node "bash ${WORK}/g5k_scripts/collect.sh"
}

nodes=($(uniq $OAR_NODEFILE))

nr_exps=${#EXPS[@]}
nr_nodes=${#nodes[@]}

if [ ${#EXPS[@]} -ne ${#nodes[@]} ]; then
	echo "There are ${nr_exps} planned experiments and ${nr_nodes} reserved nodes!" 1>&2

	exit 1
fi

# We first deploy debian with NFS on all reserved node.
kadeploy3 -f $OAR_NODEFILE -e debian10-x64-nfs -k

# Setup all the nodes.
for node in ${nodes[@]}; do
	setup $node &
done

wait

# Run each experiment on different node.
# ${!nodes[@]} permits to get the key of the array in bash only.
for i in ${!nodes[@]}; do
	run ${nodes[$i]} ${EXPS[$i]} &
done

wait

# We finally collect all the results directories into the nfs home dir.
for node in ${nodes[@]}; do
	collect $node &
done

wait