# Collection of script to run experiments on Grid'5000

This repository contains a collection of script used to run experiments and collect results on the Grid'5000 testbed.

## Getting started

Before running the experiment, you need to set multiple variables in `run.sh`:

- `NFS_DIR`: The experiment environment will be taken from this directory.
- `BZIMAGE_VANILLA`: Set this to the path of a vanilla Linux `bzImage`.
- `BZIMAGE_MODIFIED`: Same as above but for a modified one, _e.g._ with your own modifications.
- `EXPS_PATH`: Set this to the path of experiments scripts.
- `EXPS`: This array contains for each line a kernel used for the experiment and the python script of the experiment with its arguments.
Note that, the number of experiments, _i.e._ the number of lines in `$EXPS` should be equal to the number of reserved node.

When you set all this variable you can reserve a machine with:

```bash
you@grenoble:~/g5k_scripts$ oarsub -t deploy -p "cluster='dahu'" -l nodes=4,walltime=12 -k -r 'YYYY-MM-DD HH:MM:SS' 'bash run.sh' -n "experiments"
```

Replace "YYYY-MM-DD HH:MM:SS" with the date you want your experiment to run.

## How does it work?

There are 3 steps done by `run.sh`:

1. It setups the node by deploying debian 10 and put the experiment environment on the deployed node.
2. It runs one experiment per node.
3. It collects the results of the experiments from each node.

For this, it calls the others scripts.
Normally, you should not have to run of this script alone.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

## License

This project is licensed under the GPLv2 License.